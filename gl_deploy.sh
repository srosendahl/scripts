#!/usr/bin/env bash
#TODO -- version number incremetation
PAGE_ID="35487745"
FILENAME="orionbuild_"
VERSION_NUMBER="32"
VERSION_FILE="/home/gitlab-runner/version.txt"
ATLASSIAN_SERVER="https://circadence.atlassian.net/wiki/rest/api/content/$PAGE_ID/child/attachment"

function getversion() {
    echo "CHECKING VERSION..."
    VERSION_NUMBER=$(cat $VERSION_FILE | tail -1)
    VERSION_NUMBER=$(($VERSION_NUMBER + 1))
    echo -e "\n$VERSION_NUMBER" >> $VERSION_FILE
    VERSION_NUMBER=$(printf %03d $VERSION_NUMBER)
    FILENAME="$FILENAME$VERSION_NUMBER"
}

# Eclipse creates a bin folder for us, but we won't have that here, so we make it manually.
function mimic_eclipse() {
  mkdir bin
  cp -Rf build/main/hermes bin
  cp -Rf test/resources bin
}

# We need to build and zip the file. The ant file builds into ./orionbuild_xxx
function bnz() {
  echo "BUILDING AND ZIPPING..."
  ant compile
  if [ $? -eq 0 ]; then
    mimic_eclipse
  else
    echo "FAILED TO COMPILE :("
    exit 1
  fi
  ant export
  if [ $? -eq 0 ]; then
    getversion
    cp -R -f ~/orionbuild_032 $FILENAME
    tar -zcvf "$FILENAME.zip" $FILENAME
  else
    echo "FAILED TO BUILD :("
    exit 1
  fi
}

# Upload the file to Confluence with some voodo cURL magic
function ul_to() {
  echo "BEGINNING UPLOAD..."
  pwd
  UNAME=$(cat /home/gitlab-runner/uname.txt)
  PASS=$(cat /home/gitlab-runner/pw.txt)
  curl -v -S -u "$UNAME":"$PASS" -L -X POST -H "X-Atlassian-Token: no-check" -F "file=@$FILENAME.zip" $ATLASSIAN_SERVER
  rm -f "$FILENAME.zip"
}

# Handle moving the file
function mvfile() {
  echo "CONFIGURING FILE..."
  case $1 in
    -t|--test)
    TEST_STR="_TEST"
    cp "$FILENAME.zip" "$FILENAME$TEST_STR.zip"
    $FILENAME="$FILENAME$TEST_STR"
    ;;
    *)
    # cp ~/orionbuild_032.zip ./orionbuild_032.zip
    ;;
  esac
}

bnz
mvfile $1
ul_to
